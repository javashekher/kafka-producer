package com.example.producer.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class Student {
	
	@NotNull
	private Integer id;
	
	@NotEmpty
	private String name;
	
	@NotEmpty
	@Email
	private String email;

}
