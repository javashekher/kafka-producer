package com.example.producer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.producer.model.Student;
import com.example.producer.service.ProducerService;

@RestController
public class ProducerController {
	
	@Autowired
	ProducerService pService;
	
	@PostMapping(value = "/send")
	public ResponseEntity<String> sendMessage(@RequestBody @Valid Student student) {
		pService.sendMessage(student);
		return ResponseEntity.status(HttpStatus.CREATED).body("Student object is sent to kafka");
	}

}
